# PeerTube privacy remover

Remove video privacy settings of your choice.

For example you can force your users to publish all their videos in "Internal" video privacy.
Or remove the "Public" video privacy.
