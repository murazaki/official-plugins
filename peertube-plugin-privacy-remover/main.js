async function register ({
  registerSetting,
  settingsManager,
  videoPrivacyManager
}) {
  registerSetting({
    name: 'disable-video-public',
    label: 'Disable "Public" privacy',
    type: 'input-checkbox',
    private: true,
    default: false
  })

  registerSetting({
    name: 'disable-video-unlisted',
    label: 'Disable "Unlisted" protected privacy',
    type: 'input-checkbox',
    private: true,
    default: false
  })

  registerSetting({
    name: 'disable-video-internal',
    label: 'Disable "Internal" privacy',
    type: 'input-checkbox',
    private: true,
    default: false
  })

  registerSetting({
    name: 'disable-video-password-protected',
    label: 'Disable "Password protected" privacy',
    type: 'input-checkbox',
    private: true,
    default: false
  })

  registerSetting({
    name: 'disable-video-private',
    label: 'Disable "Private" privacy',
    type: 'input-checkbox',
    private: true,
    default: false
  })

  settingsManager.onSettingsChange(settings => {
    if (!settings) return

    videoPrivacyManager.resetConstants()

    const obj = [
      [ 'disable-video-public', 1 ],
      [ 'disable-video-unlisted', 2 ],
      [ 'disable-video-private', 3 ],
      [ 'disable-video-internal', 4 ],
      [ 'disable-video-password-protected', 5 ]
    ]

    for (const [ key, value ] of obj) {
      if (settings[key] === true) videoPrivacyManager.deleteConstant(value)
    }
  })
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
