async function register ({
  registerSetting,
  settingsManager,
  transcodingManager
}) {
  const defaultCRF = 20

  const store = {
    crf: await settingsManager.getSetting('crf') || defaultCRF
  }

  settingsManager.onSettingsChange(settings => {
    store.crf = settings['crf']
  })

  const builderVOD = (options) => {
    return {
      outputOptions: [
        `-r ${options.fps}`,
        `-crf ${store.crf}`
      ]
    }
  }

  const buildLive = (options) => {
    return {
      outputOptions: [
        `${buildStreamSuffix('-r:v', options.streamNum)} ${options.fps}`,
        `-crf ${store.crf}`
      ]
    }
  }

  registerSetting({
    name: 'crf',
    label: 'Quality',
    type: 'select',
    options: [
      { label: 'Low', value: 36 },
      { label: 'Medium', value: 33 },
      { label: 'Good (Peertube default)', value: 30 },
      { label: 'Very good', value: 27 },
      { label: 'Excellent', value: 24 },
      { label: 'Perfect', value: 21 },
      { label: 'Unreasonnable', value: 18 },
      { label: 'Insane', value: 15 }
    ],
    descriptionHTML: 'Increasing quality will result in bigger video sizes',
    private: true,
    default: defaultCRF
  })

  const encoder = 'libx264'
  const profileName = 'custom-quality'

  transcodingManager.addVODProfile(encoder, profileName, builderVOD)
  transcodingManager.addLiveProfile(encoder, profileName, buildLive)
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}


// ---------------------------------------------------------------------------

function buildStreamSuffix (base, streamNum) {
  if (streamNum !== undefined) {
    return `${base}:${streamNum}`
  }

  return base
}
